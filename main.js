/**
 * Created by wfournier on 2/7/15.
 */

API_BASE = 'http://192.168.54.95:5000/api/v1/';

function init() {
    document.addEventListener("deviceready", deviceInfo, true);
}

function changeFan(level) {
    console.log('changeFan called with: '+level);
    $.post( API_BASE + "fan", {'level': level})
        .done(function(result){
            if (result == "OK") {
                console.log('OK');
                return true;
            } else {
                alert("Command returned error: " + result)
            }
        })
        .fail(function(data){
            console.log(data);
            alert("Failed to send command")
            return false;
        }
    );
}

$( document ).on( "pagecreate", function( event ) {
    $(".fanButton").click(function (e) {
        //console.log('calling changeFan with: '+e.target.getAttribute('id'));
        changeFan(e.target.getAttribute('id'));
    });

    (function worker() {
        $.ajax({
            url: API_BASE + "humidity/" + '1',
            success: function(data) {
                console.log('Humidity: ' + data);
                $( "p#humidity" ).text('Current Humidity: ' + data);
                if (data > 70) {
                    $( "div#humidityHeader").css('background-color', 'red')
                } else {
                    $( "div#humidityHeader").css('background-color', '#e9e9e9')
                }
            },
            fail: function(data) {
                console.log('Humidity failed: ' + data);
                $( "p#humidity" ).text('Current Humidity: --');
            },
            complete: function() {
                // Schedule the next request when the current one is complete
                setTimeout(worker, 5000);
            }
        });
    })();
});
