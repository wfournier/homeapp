#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
from flask import Flask, jsonify, request, make_response, render_template
import json

class itho(object):
        def __init__(self):
                self.BUTTON={ 1: 22, 2: 16, 3: 18 }
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(self.BUTTON[1], GPIO.OUT)
                GPIO.setup(self.BUTTON[2], GPIO.OUT)
                GPIO.setup(self.BUTTON[3], GPIO.OUT)
                GPIO.output(self.BUTTON[1], True)
                GPIO.output(self.BUTTON[2], True)
                GPIO.output(self.BUTTON[3], True)

        def press(self,pin):
                #print("setting {pin}".format(pin=pin))
                GPIO.output(self.BUTTON[pin],False)
                time.sleep(0.5)
                #print("unsetting {pin}".format(pin=pin))
                GPIO.output(self.BUTTON[pin],True)

itho=itho()

app = Flask(__name__)
#app.debug = True

@app.errorhandler(404)
def not_found(error):
    return make_response(json.dumps({'error': 'Not found'}), 404)

@app.route('/api/v1/fan', methods=['POST'])
def fan():
    level = int(request.form['level'])
    if not (level in [1,2,3]):
        response = 'Invalid level. Use either 1 , 2 or 3'
        code = 500
    else:
        itho.press(level)
        response = 'OK'
        code = 200
    print response
    return response, code

@app.route('/')
def index():
    return render_template('index.html')
if __name__ == '__main__':
    app.run(host='0.0.0.0')

